<?php

namespace App\Security\UniqueID;


use Ramsey\Uuid\Uuid;

class UUIDGenerator implements UniqueIDGeneratorInterface
{
    public function generate(): string
    {
        return Uuid::uuid4()->toString();
    }
}