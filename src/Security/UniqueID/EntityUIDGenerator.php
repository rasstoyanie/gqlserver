<?php

namespace App\Security\UniqueID;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;

class EntityUIDGenerator extends AbstractIdGenerator
{
    /**
     * Generates an identifier for an entity.
     *
     * @param EntityManager $em
     * @param \Doctrine\ORM\Mapping\Entity $entity
     * @return string
     */
    public function generate(EntityManager $em, $entity): string
    {
        return new EntityUniqueID();
    }
}