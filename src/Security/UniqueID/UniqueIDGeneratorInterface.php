<?php

namespace App\Security\UniqueID;


interface UniqueIDGeneratorInterface
{
    public function generate(): string ;
}