<?php

namespace App\Security\UniqueID;


abstract class AbstractUniqueID
{
    private $id;
    private $generator;

    public function __construct(UniqueIDGeneratorInterface $generator = null)
    {
        $this->generator = $generator?:new Base64UIDGenerator();
        $this->id = $this->generator->generate();
    }

    public function id(): string
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return $this->id;
    }
}