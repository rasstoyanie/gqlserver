<?php

namespace App\Security\UniqueID;


use GpsLab\Component\Base64UID\Base64UID;

class Base64UIDGenerator implements UniqueIDGeneratorInterface
{
    public function generate(): string
    {
        return Base64UID::generate();
    }
}