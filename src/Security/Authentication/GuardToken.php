<?php

namespace App\Security\Authentication;


use App\Entity\InternalUser;
use App\Exception\RestrictedActionException;
use Lcobucci\JWT\Token;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Guard\Token\GuardTokenInterface;

class GuardToken implements GuardTokenInterface
{
    private $user;
    private $jwt;
    private $authenticated;


    public function __construct(InternalUser $user, Token $jwt, $authenticated = false)
    {
        $this->user = $user;
        $this->jwt = $jwt;
        $this->authenticated = $authenticated;
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize(): string
    {
        return serialize(array($this->user, $this->jwt, $this->authenticated));
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list($this->user, $this->jwt, $this->authenticated) = unserialize($serialized);
    }

    /**
     * Returns a string representation of the Token.
     *
     * This is only to be used for debugging purposes.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->serialize();
    }

    /**
     * Returns the user roles.
     *
     * @return Role[] An array of Role instances
     */
    public function getRoles(): array
    {
        return $this->getUser()->getRoles();
    }

    /**
     * Returns the user credentials.
     *
     * @return mixed The user credentials
     */
    public function getCredentials(): Token
    {
        return $this->jwt;
    }

    /**
     * Returns a user representation.
     *
     * @return mixed Can be a UserInterface instance, an object implementing a __toString method,
     *               or the username as a regular string
     *
     * @see AbstractToken::setUser()
     */
    public function getUser(): InternalUser
    {
        return $this->user;
    }

    /**
     * Sets a user.
     *
     * @param mixed $user
     */
    public function setUser($user)
    {
        throw new RestrictedActionException('You can not change token user.');
    }

    /**
     * Returns the username.
     *
     * @return string
     */
    public function getUsername(): string
    {
        return $this->getUser()->getUsername();
    }

    /**
     * Returns whether the user is authenticated or not.
     *
     * @return bool true if the token has been authenticated, false otherwise
     */
    public function isAuthenticated()
    {
        return $this->authenticated;
    }

    /**
     * Sets the authenticated flag.
     *
     * @param bool $isAuthenticated The authenticated flag
     */
    public function setAuthenticated($isAuthenticated)
    {
        $this->authenticated = $isAuthenticated;
    }

    /**
     * Removes sensitive information from the token.
     */
    public function eraseCredentials()
    {
        $this->getUser()->eraseCredentials();
        $this->jwt = null;
    }

    /**
     * Returns the token attributes.
     *
     * @return array The token attributes
     */
    public function getAttributes()
    {
        return $this->jwt->getClaims();
    }

    /**
     * Sets the token attributes.
     *
     * @param array $attributes The token attributes
     */
    public function setAttributes(array $attributes)
    {
        throw new RestrictedActionException('You can not change token attributes.');
    }

    /**
     * Returns true if the attribute exists.
     *
     * @param string $name The attribute name
     *
     * @return bool true if the attribute exists, false otherwise
     */
    public function hasAttribute($name)
    {
        return $this->jwt->hasClaim($name);
    }

    /**
     * Returns an attribute value.
     *
     * @param string $name The attribute name
     *
     * @return mixed The attribute value
     *
     * @throws \InvalidArgumentException When attribute doesn't exist for this token
     */
    public function getAttribute($name)
    {
        return $this->jwt->getClaim($name);
    }

    /**
     * Sets an attribute.
     *
     * @param string $name The attribute name
     * @param mixed $value The attribute value
     */
    public function setAttribute($name, $value)
    {
        throw new RestrictedActionException('You can not change token attributes.');
    }
}