<?php

namespace App\Security\Authentication;

use Symfony\Component\Validator\Constraints as Assert;

class LoginForm
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     */
    public $username;
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     */
    public $password;
    /**
     * @var bool
     * @Assert\NotBlank()
     * @Assert\Type(type="bool")
     */
    public $rememberMe;
}