<?php

namespace App\Security\Authentication;


use App\GraphQL\GraphQLRequestContent;
use App\GraphQL\Login\LoginMutation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AuthenticatorInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class AnonymousAuthenticator implements AuthenticatorInterface
{
    /**
     * @var GraphQLRequestContent
     */
    private $contentObject;

    public function __construct(RequestStack $requestStack)
    {
        $serializer = new Serializer(array(new ObjectNormalizer()), array(new JsonEncoder()));
        $this->contentObject = $serializer->deserialize($requestStack->getCurrentRequest()->getContent(), GraphQLRequestContent::class, 'json');
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        throw new \App\Exception\AuthenticationException();
    }

    public function supports(Request $request)
    {
        return !$request->headers->has('Authorization')
            && $this->contentObject->operationName != LoginMutation::OPERATION_NAME;
    }

    public function getCredentials(Request $request)
    {
        throw new \App\Exception\AuthenticationException();
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        return null;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        throw new \App\Exception\AuthenticationException();
    }

    public function createAuthenticatedToken(UserInterface $user, $providerKey)
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return null;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    public function supportsRememberMe()
    {
        return false;
    }
}