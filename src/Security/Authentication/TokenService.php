<?php

namespace App\Security\Authentication;

use App\Entity\InternalUser;
use App\Event\TokenGeneratedEvent;
use App\Security\UniqueID\TokenUniqueID;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\ValidationData;
use Symfony\Component\EventDispatcher\EventDispatcherInterface as EventDispatcher;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\HttpKernel\KernelInterface;

class TokenService
{
    const passphrase = 'maxroot';
    const keys_dir = '/var/jwt/';
    const issuer = 'http://example.com';
    const audience = 'http://example.com';

    /**
     * @var Kernel
     */
    private $kernel;
    private $request;
    private $dispatcher;

    private $token;
    private $changed = false;

    public function __construct(
        KernelInterface $kernel,
        RequestStack $requestStack,
        EventDispatcher $dispatcher
    )
    {
        $this->kernel = $kernel;
        $this->request = $requestStack->getMasterRequest();
        $this->dispatcher = $dispatcher;
        $this->token = $this->getTokenFromRequest();
    }

    public function generateToken(InternalUser $user): Token
    {
        $this->token = (new Builder())->setIssuer(self::issuer)
            ->setAudience(self::audience)
            ->setId(new TokenUniqueID(), true)
            ->setIssuedAt(time())
            ->setExpiration(time() + 3600)
            ->set('uid', $user->getId())
            ->sign(new Sha256(),  new Key('file://'.$this->getKeysDir().'private.pem', self::passphrase))
            ->getToken();

        $this->changed = true;

        $this->dispatcher->dispatch(TokenGeneratedEvent::NAME, new TokenGeneratedEvent($user, $this->token));

        return $this->token;
    }

    public function hasToken(): bool
    {
        return (bool) $this->token;
    }

    public function getToken(): ?Token
    {
        return $this->token;
    }

    public function isTokenChanged(): bool
    {
        return $this->changed;
    }

    public function isTokenExpired(): bool
    {
        return $this->token->isExpired();
    }

    public function validateToken($time = null): bool
    {
        return $this->token->validate($this->getValidationData(new ValidationData($time?:time())));
    }

    private function getValidationData(ValidationData $validationData)
    {
        $validationData->setIssuer(self::issuer);
        $validationData->setAudience(self::audience);

        return $validationData;
    }

    public function verifyToken()
    {
        return $this->token->verify(new Sha256(),  new Key('file://'.$this->getKeysDir().'public.pem', self::passphrase));
    }

    private function getTokenFromRequest(): ?Token
    {
        if ($this->request->headers->has('Authorization')) {
            $authParts = explode(' ', $this->request->headers->get('Authorization'));
            if (count($authParts) === 2 && $authParts[0] === 'Bearer')
                return $this->parseToken($authParts[1]);
        }
        return null;
    }

    public function parseToken(string $token): Token
    {
        return (new Parser())->parse($token);
    }

    private function getKeysDir()
    {
        return $this->kernel->getProjectDir().self::keys_dir;
    }



//    public function getTokenFromRequest(Request $request): string
//    {
//        if (!$request->headers->has('Authorization')) {
//            throw new UnauthorizedHttpException('Bearer realm="'.$request->getUri().'"');
//        }
//        $authParts = explode(' ', $request->headers->get('Authorization'));
//        if (!(count($authParts) === 2 && $authParts[0] === 'Bearer')) {
//            throw new UnauthorizedHttpException('Bearer realm="'.$request->getUri().'"');
//        }
//        return $authParts[1];
//    }
}