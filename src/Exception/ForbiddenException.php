<?php

namespace App\Exception;


class ForbiddenException extends AbstractHttpException
{
    public function __construct(
        string $message = 'Access denied.',
        int $statusCode = 403
    )
    {
        parent::__construct($message, $statusCode);
    }
}
