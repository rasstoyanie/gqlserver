<?php

namespace App\Exception;


class UnsupportedActionException extends AbstractHttpException
{
    public function __construct(
        string $message = 'Unsupported action exception.',
        int $statusCode = 400
    )
    {
        parent::__construct($message, $statusCode);
    }
}
