<?php

namespace App\Exception;


class AuthenticationException extends AbstractHttpException
{
    public function __construct(
        string $message = 'Authentication failed.',
        int $statusCode = 401
    )
    {
        parent::__construct($message, $statusCode);
    }
}
