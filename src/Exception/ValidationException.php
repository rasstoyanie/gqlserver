<?php

namespace App\Exception;

use App\GraphQL\Shared\GraphQLError;

class ValidationException extends GraphQLError
{
    public function __construct(
        string $message = 'Validation failed.',
        string $type = GraphQLError::TYPE_NOTICE
    )
    {
        parent::__construct($message, $type);
    }
}
