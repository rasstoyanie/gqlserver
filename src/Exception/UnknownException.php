<?php

namespace App\Exception;


class UnknownException extends AbstractHttpException
{
    public function __construct(
        string $message = 'Unknown exception.',
        int $statusCode = 400
    )
    {
        parent::__construct($message, $statusCode);
    }
}
