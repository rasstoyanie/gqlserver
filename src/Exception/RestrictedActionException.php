<?php

namespace App\Exception;


class RestrictedActionException extends AbstractHttpException
{
    public function __construct(
        string $message = 'Restricted action exception.',
        int $statusCode = 400
    )
    {
        parent::__construct($message, $statusCode);
    }
}
