<?php

namespace App\Exception;


class InvalidArgumentException extends AbstractHttpException
{
    public function __construct(
        string $message = 'Invalid argument exception.',
        int $statusCode = 400
    )
    {
        parent::__construct($message, $statusCode);
    }
}
