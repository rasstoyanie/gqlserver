<?php

namespace App\Exception;


interface TypedExceptionInterface
{
    public function __construct(string $message, string $type);

    public function getType(): string;
}
