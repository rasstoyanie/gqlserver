<?php

namespace App\Exception;

use App\GraphQL\Shared\GraphQLError;

class UserNotFoundException extends GraphQLError
{
    public function __construct(
        string $message = 'User not found.',
        string $type = GraphQLError::TYPE_ERROR
    )
    {
        parent::__construct($message, $type);
    }
}
