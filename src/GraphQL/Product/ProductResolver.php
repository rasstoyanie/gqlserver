<?php

namespace App\GraphQL\Product;

use App\Component\Datagrid\DatagridState;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ProductResolver implements ResolverInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param array $datagridState
     * @return Product[]
     */
    public function productList(array $datagridState): array
    {
        $serializer = new Serializer([
            new ObjectNormalizer(null, null, null, new PhpDocExtractor()),
            new ArrayDenormalizer()
        ]);
        /**
         * @var $state DatagridState
         */
        $state = $serializer->denormalize($datagridState, DatagridState::class);

        return $this->entityManager->getRepository(Product::class)
//            ->createQueryBuilder('p')
//            ->where('p.OrderEmail = :email')
//            ->andWhere('o.Product LIKE :product')
//            ->setParameter('email', 'some@mail.com')
//            ->setParameter('product', 'My Products%')
//            ->getQuery()
//            ->getResult();
                    ->findBy($state->getCriteria(),$state->getSort(),$state->page->size,$state->page->from);
    }

    public function productById(string $id): ?Product
    {
        return $this->entityManager->getRepository(Product::class)->find($id);
    }

    public function userByName(string $name): ?Product
    {
        return $this->entityManager->getRepository(Product::class)->findOneBy(['name'=>$name]);
    }
}