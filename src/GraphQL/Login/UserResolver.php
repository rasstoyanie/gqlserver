<?php

namespace App\GraphQL\Login;

use App\Entity\InternalUser;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;

class UserResolver implements ResolverInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return InternalUser[]
     */
    public function userList(): array
    {
        return $this->entityManager->getRepository(InternalUser::class)->findBy([],[],10);
    }

    public function userById(string $id): ?InternalUser
    {
        return $this->entityManager->getRepository(InternalUser::class)->find($id);
    }

    public function userByUsername(string $username): ?InternalUser
    {
        return $this->entityManager->getRepository(InternalUser::class)->findOneBy(['username'=>$username]);
    }
}