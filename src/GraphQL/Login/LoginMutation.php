<?php

namespace App\GraphQL\Login;

use App\Exception\ValidationException;
use App\Security\Authentication\LoginForm;
use App\Security\Authentication\TokenService;
use App\Service\ErrorCollector;
use App\Service\ValidatorService;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class LoginMutation implements MutationInterface
{
    const OPERATION_NAME = 'loginMutation';

    private $userResolver;
    private $tokenService;
    private $validatorService;
    private $errorCollector;

    public function __construct(
        UserResolver $userResolver,
        TokenService $tokenService,
        ValidatorService $validatorService,
        ErrorCollector $errorCollector
    )
    {
        $this->userResolver = $userResolver;
        $this->tokenService = $tokenService;
        $this->validatorService = $validatorService;
        $this->errorCollector = $errorCollector;
    }

    /**
     * @param array $form
     * @return array
     * @throws \Exception
     */
    public function login(array $form): array
    {
        $serializer = new Serializer([new ObjectNormalizer()]);
        /**
         * @var $loginForm LoginForm
         */
        $loginForm = $serializer->denormalize($form, LoginForm::class);

//        $errors = $this->validatorService->validate($loginForm);
//
//        if(count($errors) > 0)
//            $this->errorCollector->addError(new ValidationException());

        $user = $this->userResolver->userByUsername($loginForm->username);

        return [
            'user' => $user,
        ];
    }
}