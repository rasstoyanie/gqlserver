<?php

namespace App\GraphQL\Login;

use App\Entity\InternalUser;
use App\Security\Authentication\TokenService;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;

class TokenResolver implements ResolverInterface
{
    private $tokenService;

    public function __construct(TokenService $tokenService)
    {
        $this->tokenService = $tokenService;
    }

    public function tokenByUser(InternalUser $user): string
    {
        return $this->tokenService->generateToken($user);
    }
}