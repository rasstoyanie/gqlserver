<?php

namespace App\GraphQL;


class GraphQLRequestContent
{
    public $operationName;
    public $query;
    public $variables;
}