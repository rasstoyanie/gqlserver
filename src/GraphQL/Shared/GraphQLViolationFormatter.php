<?php

namespace App\GraphQL\Shared;

class GraphQLViolationFormatter
{
    public function __invoke(GraphQLViolation $violation): array
    {
        return [
            'message'=>$violation->getMessage(),
        ];
    }
}