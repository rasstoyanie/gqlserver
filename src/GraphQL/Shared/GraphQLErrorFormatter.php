<?php

namespace App\GraphQL\Shared;


use App\Exception\TypedExceptionInterface;

class GraphQLErrorFormatter
{
    private $debug;
    private $responseArray;

    public function __construct(bool $debug = false)
    {
        $this->debug = $debug;
    }

    /**
     * @param \Exception $error
     * @return array
     */
    public function __invoke(\Exception $error): array
    {
        $error = $error->getPrevious()?:$error;
        $this->responseArray = [
            'message'=>$error->getMessage(),
            'type'=>GraphQLError::TYPE_ERROR
        ];
        if($error instanceof TypedExceptionInterface)
            $this->responseArray['type'] = $error->getType();

        if($this->debug)
            $this->addDebugData($error);

        return $this->responseArray;
    }

    private function addDebugData(\Exception $error)
    {
        $this->responseArray['file'] = $error->getFile();
        $this->responseArray['line'] = $error->getLine();
        $this->responseArray['trace'] = $error->getTraceAsString();
    }
}