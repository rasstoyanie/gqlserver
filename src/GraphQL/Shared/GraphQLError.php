<?php

namespace App\GraphQL\Shared;

use App\Exception\TypedExceptionInterface;
use GraphQL\Error\Error;

abstract class GraphQLError extends Error implements TypedExceptionInterface
{
    const TYPE_ERROR = 'danger';
    const TYPE_WARNING = 'warning';
    const TYPE_NOTICE = 'info';

    private $type;

    public function __construct(
        string $message,
        string $type = null
    )
    {
        $this->type = $type?:static::TYPE_ERROR;

        parent::__construct($message);
    }

    public function getType(): string
    {
        return $this->type;
    }
}