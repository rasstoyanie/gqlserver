<?php

namespace App\GraphQL\Shared;


class GraphQLErrorsHandler
{
    /**
     * @param \Exception[] $errors
     * @param callable $formatter
     * @return array
     */
    public function __invoke(array $errors, callable $formatter): array
    {
        return array_map($formatter, $errors);
    }
}