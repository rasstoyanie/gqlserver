<?php

namespace App\GraphQL\Shared;


use Symfony\Component\Validator\ConstraintViolation;

abstract class GraphQLViolation extends ConstraintViolation
{

}