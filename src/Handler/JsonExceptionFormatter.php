<?php

namespace App\Handler;


class JsonExceptionFormatter implements ExceptionFormatterInterface
{
    private $data;

    public function format(\Exception $exception, bool $debug = false)
    {
        $this->data = $this->getData($exception);

        if($debug)
            $this->addDebugData($exception);

        return $this->data;
    }

    private function getData(\Exception $exception): array
    {
        return [
            'message'=>$exception->getMessage(),
        ];
    }

    private function addDebugData(\Exception $exception)
    {
        $this->data['file'] = $exception->getFile();
        $this->data['line'] = $exception->getLine();
        $this->data['trace'] = $exception->getTraceAsString();
    }
}