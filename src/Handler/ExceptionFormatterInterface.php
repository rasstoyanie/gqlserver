<?php

namespace App\Handler;


interface ExceptionFormatterInterface
{
    public function format(\Exception $exception, bool $debug = false);
}