<?php

namespace App\Handler;


use App\GraphQL\Shared\GraphQLError;
use Symfony\Component\HttpFoundation\JsonResponse;

class ExceptionHandler
{
    private $formatter;

    public function __construct(ExceptionFormatterInterface $formatter)
    {
        $this->formatter = $formatter;
    }

    public function handle(\Exception $exception, bool $debug = false): JsonResponse
    {
        return new JsonResponse($this->formatter->format($exception, $debug), $exception->getCode()?:400, $this->getHeaders());
    }

    public function getHeaders(): array
    {
        return [
            'Content-Type' => 'application/json'
        ];
    }

    public function setFormatter(ExceptionFormatterInterface $formatter)
    {
        $this->formatter = $formatter;
    }

}