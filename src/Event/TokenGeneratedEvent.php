<?php

namespace App\Event;

use App\Entity\InternalUser;
use Lcobucci\JWT\Token;
use Symfony\Component\EventDispatcher\Event;

class TokenGeneratedEvent extends Event
{
    const NAME = 'token.generated';

    protected $user;
    protected $token;

    public function __construct(InternalUser $user, Token $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    public function getUser(): InternalUser
    {
        return $this->user;
    }

    public function getToken(): Token
    {
        return $this->token;
    }
}