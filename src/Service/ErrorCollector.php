<?php

namespace App\Service;


class ErrorCollector
{
    /**
     * @var \Exception[]
     */
    private $errors = [];

    public function addError(\Exception $error): self
    {
        array_unshift($this->errors, $error);
        return $this;
    }

    /**
     * @return \Exception[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}