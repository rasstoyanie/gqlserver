<?php

namespace App\Component\Datagrid;

use Symfony\Component\Validator\Constraints as Assert;

class Sort
{
    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\NotBlank()
     */
    public $by;
    /**
     * @var bool
     * @Assert\Type(type="bool")
     * @Assert\NotBlank()
     */
    public $reverse = false;

    public function getDirection(): string
    {
        return $this->reverse?'DESC':'ASC';
    }
}