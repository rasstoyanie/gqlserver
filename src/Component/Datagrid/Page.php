<?php

namespace App\Component\Datagrid;

use Symfony\Component\Validator\Constraints as Assert;

class Page
{
    /**
     * @var int
     * @Assert\Type(type="int")
     */
    public $from;
    /**
     * @var int
     * @Assert\Type(type="int")
     */
    public $to;
    /**
     * @var int
     * @Assert\Type(type="int")
     */
    public $size;
}