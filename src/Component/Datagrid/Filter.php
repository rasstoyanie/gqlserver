<?php

namespace App\Component\Datagrid;

use Symfony\Component\Validator\Constraints as Assert;

class Filter
{
    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\NotBlank()
     */
    public $property;
    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\NotBlank()
     */
    public $value;
}