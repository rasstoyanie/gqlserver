<?php

namespace App\Component\Datagrid;

use Symfony\Component\Validator\Constraints as Assert;

class DatagridState
{
    /**
     * @var Page
     * @Assert\Type(type="object")
     */
    public $page;
    /**
     * @var Sort
     * @Assert\Type(type="object")
     */
    public $sort;
    /**
     * @var Filter[]
     * @Assert\Type(type="array")
     */
    public $filters = [];

    public function getCriteria(): array
    {
        $criteria = array_reduce($this->filters, function (array $array, Filter $item){
            $array[$item->property] = $item->value;
            return $array;
        }, []);

        return $criteria;
    }

    public function getSort(): array
    {
        return $this->sort?[$this->sort->by => $this->sort->getDirection()]:[];
    }
}