<?php

namespace App\EventSubscriber;

use App\Security\Authentication\TokenService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ResponseEventSubscriber implements EventSubscriberInterface
{
    private $tokenService;
    private $tokenStorage;

    public function __construct(TokenService $tokenService, TokenStorageInterface $tokenStorage)
    {
        $this->tokenService = $tokenService;
        $this->tokenStorage = $tokenStorage;
    }

    public static function getSubscribedEvents()
    {
        return array(
//            KernelEvents::RESPONSE => [
//                'updateToken'
//            ]
        );
    }

    public function updateToken(FilterResponseEvent $event)
    {
        if(!$this->tokenService->validateToken(time()+900)) {

        }
    }

}