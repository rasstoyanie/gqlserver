<?php

namespace App\EventSubscriber;

use App\Event\TokenGeneratedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AuthEventSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            TokenGeneratedEvent::NAME => 'onTokenGenerated'
        );
    }

    public function onTokenGenerated(TokenGeneratedEvent $event)
    {

    }
}