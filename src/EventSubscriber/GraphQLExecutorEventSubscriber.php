<?php

namespace App\EventSubscriber;

use App\GraphQL\Shared\GraphQLErrorFormatter;
use App\GraphQL\Shared\GraphQLErrorsHandler;
use App\Security\Authentication\TokenService;
use App\Service\ErrorCollector;
use App\Service\ValidatorService;
use Overblog\GraphQLBundle\Event\ExecutorResultEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class GraphQLExecutorEventSubscriber implements EventSubscriberInterface
{
    private $errorCollector;
    private $validatorService;
    private $tokenService;

    public function __construct(
        ErrorCollector $errorCollector,
        ValidatorService $validatorService,
        TokenService $tokenService
    )
    {
        $this->errorCollector = $errorCollector;
        $this->validatorService = $validatorService;
        $this->tokenService = $tokenService;
    }

    public static function getSubscribedEvents()
    {
        return array(
            'graphql.post_executor' => [
                'onPostExecutor'
            ]
        );
    }

    public function onPostExecutor(ExecutorResultEvent $event)
    {
        if($event->getResult()->errors)
            $this->errorCollector->addError(array_pop($event->getResult()->errors));
        if($this->errorCollector->getErrors()) {
            $event->getResult()->setErrorsHandler(new GraphQLErrorsHandler());
            $event->getResult()->setErrorFormatter(new GraphQLErrorFormatter($_SERVER['APP_ENV'] === 'dev'));
            $event->getResult()->errors = $this->errorCollector->getErrors();
        }

        if($this->tokenService->hasToken() && $this->tokenService->isTokenChanged())
            $event->getResult()->extensions['token'] = (string) $this->tokenService->getToken();

//        $event->getResult()->extensions['violations'] = [
//            [
//                'form'=>'loginForm',
//                'errors'=>[
//                    ['message'=>'test form error']
//                ],
//                'controls'=>[
//                    'control'=>'loginFormControl',
//                    'errors'=>[
//                        ['message'=>'test control error']
//                    ]
//                ]
//            ]
//        ];

//        $event->getResult()->extensions['credits'] = 'This api was powered by "OverblogGraphQLBundle".';
    }

}