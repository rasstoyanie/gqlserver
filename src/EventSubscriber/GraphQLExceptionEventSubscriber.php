<?php

namespace App\EventSubscriber;

use App\Service\ErrorCollector;
use Overblog\GraphQLBundle\Event\ErrorFormattingEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class GraphQLExceptionEventSubscriber implements EventSubscriberInterface
{
    private $errorCollector;

    public function __construct(ErrorCollector $errorCollector)
    {
        $this->errorCollector = $errorCollector;
    }

    public static function getSubscribedEvents()
    {
        return array(
//            'graphql.error_formatting' => [
//                'onErrorFormatting'
//            ]
        );
    }

    public function onErrorFormatting(ErrorFormattingEvent $event)
    {
        $error = $event->getError();
        if ($error->getPrevious()) {
            $code = $error->getPrevious()->getCode();
            $message = $error->getPrevious()->getMessage();
        } else {
            $code = $error->getCode();
            $message = $error->getMessage();
        }
        $formattedError = $event->getFormattedError();
        $formattedError->offsetSet('code', $code);
        $formattedError->offsetSet('message', $message);
        $formattedError->offsetUnset('locations');
        $formattedError->offsetUnset('path');
    }
}