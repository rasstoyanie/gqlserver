<?php

namespace App\EventSubscriber;

use App\Exception\AuthenticationException;
use App\Service\TokenService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class RequestEventSubscriber implements EventSubscriberInterface
{
    private $tokenService;

    public function __construct(TokenService $tokenService)
    {
        $this->tokenService = $tokenService;
    }

    public static function getSubscribedEvents()
    {
        return array(
//            KernelEvents::REQUEST => [
//                'checkTokenExists',
//                'verifyTokenExpiration',
//                'validateToken',
//                'verifyToken'
//            ]
        );
    }

    public function checkTokenExists(GetResponseEvent $event)
    {
        if(!$this->tokenService->getToken())
            throw new UnauthorizedHttpException('Bearer realm="'.$event->getRequest()->getUri().'"');
    }

    public function verifyTokenExpiration(GetResponseEvent $event)
    {
        if($this->tokenService->isTokenExpired())
            throw new AuthenticationException('Token expired.');
    }

    public function validateToken(GetResponseEvent $event)
    {
        if($this->tokenService->validateToken())
            throw new AuthenticationException('Token incorrect.');
    }

    public function verifyToken(GetResponseEvent $event)
    {
        if($this->tokenService->verifyToken())
            throw new AuthenticationException('Token verification failed.');
    }
}