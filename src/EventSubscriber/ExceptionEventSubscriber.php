<?php

namespace App\EventSubscriber;

use App\Handler\ExceptionHandler;
use App\Handler\JsonExceptionFormatter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionEventSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::EXCEPTION => [
                'throwJsonException'
            ]
        );
    }

    public function throwJsonException(GetResponseForExceptionEvent $event)
    {
        $handler = new ExceptionHandler(new JsonExceptionFormatter());
        $event->setResponse($handler->handle($event->getException(), $_SERVER['APP_ENV'] === 'dev'));
    }
}